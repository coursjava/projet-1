package com.doranco.multitiers.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class GenericDAO<T> {

	EntityManagerFactory emf = Persistence.createEntityManagerFactory("library");	
	EntityManager em = emf.createEntityManager();
	EntityTransaction transaction = em.getTransaction();
	
	public T createOrUpdate(T toBeCreated) {
		
		transaction.begin();
		em.persist(toBeCreated);
		em.flush();
		transaction.commit();
		return toBeCreated;
		
	}
	
	public void delete(T toBeDeleted) {
		transaction.begin();
		em.remove(toBeDeleted);
		transaction.commit();
	}
	
	public T find(Class clazz, Integer primarykey) {
		return(T) em.find(clazz,  primarykey);
	}
	
//	public List<T> findAll(class clazz){
//		em.
//	}
	
	public T update(T toBeUpdated) {
		transaction.begin();
		em.merge(toBeUpdated);
		transaction.commit();
		return toBeUpdated;
	}
}
