package com.doranco.multitiers.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "l_order")
public class Order extends Identifier implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Order() {
		super();
	}
	
	@ManyToOne
	private User user;	
	
	@OneToMany(mappedBy="order",fetch=FetchType.LAZY)
	private List<OrderLine> lines; 

	
	
	
	}
