package com.doranco.multitiers.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class Identifier {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	public Identifier(long id) {
		super();
		this.id = id;
	}

	public Identifier() {
		// TODO Auto-generated constructor stub
	}

}
