//package com.doranco.multitiers.dao.test;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertNotNull;
//import static org.junit.Assert.fail;
//
//import javax.ejb.EJB;
//import javax.persistence.EntityManager;
//import javax.persistence.EntityManagerFactory;
//import javax.persistence.EntityTransaction;
//import javax.persistence.Persistence;
//
//import org.apache.log4j.Logger;
//import org.jboss.arquillian.container.test.api.Deployment;
//import org.jboss.arquillian.junit.Arquillian;
//import org.jboss.arquillian.junit.InSequence;
//import org.jboss.shrinkwrap.api.ShrinkWrap;
//import org.jboss.shrinkwrap.api.asset.EmptyAsset;
//import org.jboss.shrinkwrap.api.spec.JavaArchive;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import com.doranco.multitiers.dao.CartDAO;
//import com.doranco.multitiers.entity.Cart;
//
//@RunWith(Arquillian.class)
//public class CartDAOTest {
//
//	@Deployment
//	public static JavaArchive createDeployment() {
//		return ShrinkWrap.create(JavaArchive.class, "test.jar").addClass(CartDAO.class)
//		 .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
//	}
//
//	Logger logger = Logger.getLogger(CartDAOTest.class);
//
//	@EJB
//	CartDAO dao;
//
//	@Test
//	@InSequence(1)
//	public void shouldGenerateACart() {
//		try {
//
//			Cart cart;
//			for (int i = 0; i <= 20; i++) {
//				cart = new Cart();
//				cart.setName("ligne ajouté" + i);
//				dao.createOrUpdate(cart);
//			}
//		} catch (Exception e) {
//			fail("Ne devrait pas retourner d'exception");
//		}
//	}
//
//	@Test
//	@InSequence(2)
//
//	public void shouldfindAnItem() {
//		Cart foundCart = dao.find(Cart.class, 260);
//		assertNotNull(foundCart);
//	}
//
//
//
//	@Test
//	@InSequence(3)
//	public void shouldUpdateACart() {
//		Cart foundCart = dao.find(Cart.class,185);
//		if (foundCart != null) {
//			logger.debug("Le panier d'id 265 est : " + foundCart);
//			foundCart.setName("lecture ACE");
//			dao.update(foundCart);
//			logger.debug("Le paner d'id 15 est : " + foundCart);
//		}
//		Cart updatedCart = dao.find(Cart.class, 264);
//		assertEquals(updatedCart.getName(),"lecture ACE");
//	}
//
//	@Test
//	@InSequence(4)
//	public void shouldDeletedACart1() {
//		Cart foundCart = dao.find(Cart.class, 180);
//		logger.debug("Le panier d'id 180 est : " + foundCart);
//		dao.delete(foundCart);
//		foundCart = dao.find(Cart.class, 15);
//		assertNotNull(foundCart);
//	}
//}
